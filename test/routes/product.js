let mongoose = require("mongoose");
let Users = require("../../models/api/v1/users");
let Product = require("../../models/api/v1/product");

var app = require('../../app');
const faker = require('faker');
var fs = require('fs');
var chai = require('chai');
var chaiHttp = require('chai-http');
var expect = chai.expect;
var should = chai.should;

var file = '../yagami.jpg'

chai.use(chaiHttp);

var user_merchant2 =  {
    username        : 'merchant2',
    email           : faker.internet.email(),
    password        : 'merchant2',
    first_name      : faker.name.firstName(),
    last_name       : faker.name.lastName(),
    user_type       : 'merchant'
}

var user_merchant2_login =  {
    username        : 'merchant2',
    password        : 'merchant2'
}

var token = '';

describe('Product', () => {

    beforeEach((done)=>{
        Users.deleteMany({}, (err) => {
            done();
        });
    });

    beforeEach((done)=>{
        Product.deleteMany({}, (err) => {
            done();
        });
    })

    beforeEach((done)=>{
        chai.request(app)
            .post('/api/v1/users')
            .send(user_merchant2)
            .end(function (err, data) {
                result_register = data
                done()
            })
    })
    
    beforeEach(done => {
        chai.request(app)
            .post('/api/v1/users/auth')
            .send(user_merchant2_login)
            .end(function (err, res) {
                token = res.body.token
                done()
            })
    })

    after((done)=>{
        Product.deleteMany({}, (err) => {
            done();
        });
    })

    after((done)=>{
        Users.deleteMany({}, (err) => {
            done();
        });
    })

    describe('/POST product', () => {
        let itemProduct = {
            name_product: "Kursi",
            desc_product: "asdf adsfasdf",
            price: 4000,
            stock: 300
        }

        it('it should POST succcess', (done) => {
            chai.request(app)
                .post('/api/v1/product')
                .set('authorization', token)
                .send(itemProduct)
                .end((err, res) => {
                    expect(res).to.have.status(201);
                    expect(res).to.be.a('object');

                    done();
                });
        });

        let itemProduct_err = {
            desc_product: "asdf adsfasdf",
            price: 4000,
            stock: 300
        }

        it('it should POST error', (done) => {
            chai.request(app)
                .post('/api/v1/product')
                .set('authorization', token)
                .send(itemProduct_err)
                .end((err, res) => {
                    expect(res).to.have.status(422);
                    expect(res).to.be.a('object');

                    done();
                });
        });
    });

    describe('/GET product', () => {
        it('it should GET seed test', (done) => {
            chai.request(app)
                .get('/api/v1/product/seed')
                .set('authorization', token)
                .end((err, res) => {
                    expect(res).to.have.status(201);
                    expect(res).to.be.a('object');

                    done();
                });
        });
    });

    describe('/GET product', () => {
        it('it should GET all the product', (done) => {
            chai.request(app)
                .get('/api/v1/product')
                .set('authorization', token)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res).to.be.a('object');

                    done();
                });
        });
    });

    describe('/DELETE/:id product', () => {
        it('should delete a product by single id', function(done) {
            let product_delete = new Product({
                name_product: "Kursi",
                desc_product: "asdf adsfasdf",
                price: 4000,
                stock: 300
            })

            chai.request(app)
                .post('/api/v1/product')
                .set('authorization', token)
                .send(product_delete)
                .end((err, res) => {
                    
                    chai.request(app)
                        .delete('/api/v1/product/' + res.body.results._id)
                        .set('authorization', token)
                        .end((err, res) => {
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.a('object');

                            done();
                        });

                });

        });

        it('should delete a product by wrong id', function(done) {
            chai.request(app)
                        .delete('/api/v1/product/ksdghjsfhjsaddf')
                        .set('authorization', token)
                        .end((err, res) => {
                            expect(res).to.have.status(422);
                            expect(res.body).to.be.a('object');

                            done();
                        });
        });

    });

    /* describe('/PUT image product', () => {
        let itemProduct = {
            name_product: "Kursi",
            desc_product: "asdf adsfasdf",
            price: 4000,
            stock: 300
        }

        it('it should PUT succcess', (done) => {
            chai.request(app)
                .post('/api/v1/product')
                .set('authorization', token)
                .send(itemProduct)
                .end((err, res) => {
                    console.log(res.body.results._id)

                    chai.request(app)
                        .put('/api/v1/prod-img/'+res.body.results._id)
                        .set('authorization', token)
                        .attach('picture', fs.readFileSync(`${file}`), 'yagami.jpg')
                        .end((err, res) => {
                            expect(res).to.have.status(200);
                            expect(res).to.be.a('object');

                            done();
                        });

                    done();
                });
        });
    }) */

});