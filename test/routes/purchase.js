let mongoose = require("mongoose");
let Users = require("../../models/api/v1/users");
let Product = require("../../models/api/v1/product");
let Purchase = require("../../models/api/v1/purchase");
var Purchase_detail = require('../../models/api/v1/purchase_detail');

var app = require('../../app');
const faker = require('faker');
var chai = require('chai');
var chaiHttp = require('chai-http');
var expect = chai.expect;
var should = chai.should;

chai.use(chaiHttp);

var user_merchant =  {
    username        : 'mercant2',
    email           : faker.internet.email(),
    password        : 'mercant2',
    first_name      : faker.name.firstName(),
    last_name       : faker.name.lastName(),
    user_type       : 'merchant'
}

var user_merchant_login =  {
    username        : 'mercant2',
    password        : 'mercant2'
}

var user_customer =  {
    username        : 'customer2',
    email           : faker.internet.email(),
    password        : 'customer2',
    first_name      : faker.name.firstName(),
    last_name       : faker.name.lastName(),
    user_type       : 'customer'
}

var user_customer_login =  {
    username        : 'customer2',
    password        : 'customer2'
}

var token_merchant  = '';
var token_customer  = '';
var id_product      = '';

describe('Purchase', () => {

    beforeEach((done)=>{
        Users.deleteMany({}, (err) => {
            Product.deleteMany({}, (err) => {
                Purchase.deleteMany({}, (err) => {
                    Purchase_detail.deleteMany({}, (err) => {
                        done();
                    });
                });
            });
        });
    });

    //create user_merchant
    beforeEach((done)=>{
        chai.request(app)
            .post('/api/v1/users')
            .send(user_merchant)
            .end(function (err, data) {
                result_register = data
                done()
            })
    })
    
    beforeEach((done) => {
        chai.request(app)
            .post('/api/v1/users/auth')
            .send(user_merchant_login)
            .end(function (err, res) {
                token_merchant = res.body.token
                //console.log(token_merchant)
                done()
            })
    })
    //end create user_merchant

    //create user_customer
    beforeEach((done)=>{
        chai.request(app)
            .post('/api/v1/users')
            .send(user_customer)
            .end(function (err, data) {
                result_register = data
                done()
            })
    })
    
    beforeEach((done) => {
        chai.request(app)
            .post('/api/v1/users/auth')
            .send(user_customer_login)
            .end(function (err, res) {
                token_customer = res.body.token
                //console.log(token_customer)
                done()
            })
    })
    //end create user_customer

    //create product
    beforeEach((done) => {
        let itemProduct = {
            name_product: "Kursi",
            desc_product: "asdf adsfasdf",
            price: 4000,
            stock: 300
        }

        chai.request(app)
            .post('/api/v1/product')
            .set('authorization', token_merchant)
            .send(itemProduct)
            .end((err, res) => {
                id_product = res.body.results._id

                done();
            });
    })
    //end create product

    after((done)=>{
        Users.deleteMany({}, (err) => {
            Product.deleteMany({}, (err) => {
                Purchase.deleteMany({}, (err) => {
                    Purchase_detail.deleteMany({}, (err) => {
                        done();
                    });
                });
            });
        });
    })

    describe('/POST cart', () => {

        it('it should POST succcess', (done) => {
            
            let itemCart = {
                id_product: id_product,
                qty: 200
            }
            chai.request(app)
                .post('/api/v1/purchase/cart')
                .set('authorization', token_customer)
                .send(itemCart)
                .end((err, res) => {
                    expect(res).to.have.status(201);
                    expect(res).to.be.a('object');
                    
                    done();
                });

        });

        it('it should POST error', (done) => {
            let itemCart_err = {
                id_product: id_product
            }
            chai.request(app)
                .post('/api/v1/purchase/cart')
                .set('authorization', token_customer)
                .send(itemCart_err)
                .end((err, res) => {
                    expect(res).to.have.status(422);
                    expect(res).to.be.a('object');

                    done();
                });
        });

    });

    describe('/GET cart', () => {

        it('it should GET success', (done) => {
            chai.request(app)
                .get('/api/v1/purchase/cart')
                .set('authorization', token_customer)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res).to.be.a('object');

                    done();
                });
        });

    });

    describe('/DELETE cart', () => {

        it('it should DELETE success', (done) => {

            let itemCart = {
                id_product: id_product,
                qty: 200
            }
            chai.request(app)
                .post('/api/v1/purchase/cart')
                .set('authorization', token_customer)
                .send(itemCart)
                .end((err, res) => {
                    
                    console.log(res.body.results._id)
                    chai.request(app)
                        .del('/api/v1/purchase/cart/'+res.body.results._id)
                        .set('authorization', token_customer)
                        .end((err, res) => {
                            expect(res).to.have.status(200);
                            expect(res).to.be.a('object');

                            done();
                        });

                });

        });

        it('it should DELETE error', (done) => {

            chai.request(app)
                .del('/api/v1/purchase/cart/asdfkxzcgvkxjv')
                .set('authorization', token_customer)
                .end((err, res) => {
                    expect(res).to.have.status(422);
                    expect(res).to.be.a('object');

                    done();
                });

        });

    });

    describe('/UPDATE cart', () => {

        it('it should UPDATE success', (done) => {

            let itemCart = {
                id_product: id_product,
                qty: 200
            }
            chai.request(app)
                .post('/api/v1/purchase/cart')
                .set('authorization', token_customer)
                .send(itemCart)
                .end((err, res) => {
                    
                    let data_update = {
                        qty: 200
                    }
                    chai.request(app)
                        .put('/api/v1/purchase/cart/'+res.body.results._id)
                        .set('authorization', token_customer)
                        .send(data_update)
                        .end((err, res) => {
                            expect(res).to.have.status(200);
                            expect(res).to.be.a('object');

                            done();
                        });

                });

        });

        it('it should UPDATE error', (done) => {

            let itemCart = {
                id_product: id_product,
                qty: 200
            }
            chai.request(app)
                .post('/api/v1/purchase/cart')
                .set('authorization', token_customer)
                .send(itemCart)
                .end((err, res) => {
                    
                    let data_update = {
                        qty: 200
                    }
                    chai.request(app)
                        .put('/api/v1/purchase/cart/xvjshdfkjahfjk')
                        .set('authorization', token_customer)
                        .send(data_update)
                        .end((err, res) => {
                            expect(res).to.have.status(422);
                            expect(res).to.be.a('object');

                            done();
                        });

                });

        });

    });

    describe('/POST purchase', () => {

        it('it should POST succcess', (done) => {
            
            let itemCart = {
                id_product: id_product,
                qty: 200
            }
            chai.request(app)
                .post('/api/v1/purchase/cart')
                .set('authorization', token_customer)
                .send(itemCart)
                .end((err, res) => {
                    
                    let data_purchase = {
                        payment: 100000000
                    }
                    chai.request(app)
                        .post('/api/v1/purchase/')
                        .set('authorization', token_customer)
                        .send(data_purchase)
                        .end((err, res) => {
                            expect(res).to.have.status(201);
                            expect(res).to.be.a('object');
                            
                            done();
                        })

                });

        });

        it('it should POST error', (done) => {
            
            let itemCart = {
                id_product: id_product,
                qty: 200
            }
            chai.request(app)
                .post('/api/v1/purchase/cart')
                .set('authorization', token_customer)
                .send(itemCart)
                .end((err, res) => {
                    
                    let data_purchase = {
                        paymentooo: 100000000
                    }
                    chai.request(app)
                        .post('/api/v1/purchase/')
                        .set('authorization', token_customer)
                        .send(data_purchase)
                        .end((err, res) => {
                            expect(res).to.have.status(422);
                            expect(res).to.be.a('object');
                            
                            done();
                        })
                        
                });

        });

    });

    describe('/GET history purchase', () => {

        it('it should GET succcess', (done) => {
            
            let itemCart = {
                id_product: id_product,
                qty: 200
            }
            chai.request(app)
                .post('/api/v1/purchase/cart')
                .set('authorization', token_customer)
                .send(itemCart)
                .end((err, res) => {
                    
                    let data_purchase = {
                        payment: 100000000
                    }
                    chai.request(app)
                        .post('/api/v1/purchase/')
                        .set('authorization', token_customer)
                        .send(data_purchase)
                        .end((err, res) => {

                            chai.request(app)
                                .get('/api/v1/purchase/history')
                                .set('authorization', token_customer)
                                .end((err, res) => {
                                    expect(res).to.have.status(200);
                                    expect(res).to.be.a('object');

                                    done();
                                });
                                
                        })

                });

        });

        /* it('it should POST error', (done) => {
            
            let itemCart = {
                id_product: id_product,
                qty: 200
            }
            chai.request(app)
                .post('/api/v1/purchase/cart')
                .set('authorization', token_customer)
                .send(itemCart)
                .end((err, res) => {
                    
                    let data_purchase = {
                        paymentooo: 100000000
                    }
                    chai.request(app)
                        .post('/api/v1/purchase/')
                        .set('authorization', token_customer)
                        .send(data_purchase)
                        .end((err, res) => {
                            expect(res).to.have.status(422);
                            expect(res).to.be.a('object');
                            
                            done();
                        })
                        
                });

        }); */

    });

});





