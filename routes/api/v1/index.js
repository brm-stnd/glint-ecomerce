var express = require('express');
var router = express.Router();
const usersRouter = require('./users');
const productRouter = require('./product');
const purchaseRouter = require('./purchase');


router.use('/users', usersRouter);
router.use('/product', productRouter);
router.use('/purchase', purchaseRouter);

router.get('/', function (req, res, next) {
  res.render('index', { title: 'API V1' });
});

module.exports = router;
