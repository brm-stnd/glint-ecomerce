var express = require('express');
var router = express.Router();
var productController = require('../../../controllers/api/v1/productController.js');
const auth = require('../../../middleware/auth');

router.get('/seed', auth.isAuthenticated, productController.seedProduct);

router.get('/', productController.getProduct)
    .post('/', auth.isAuthenticated, productController.insertProduct)
router.delete('/:id', auth.isAuthenticated, productController.deleteProduct)
    .get('/:id', productController.getProductbyId)
router.put('/prod-img/:id', auth.isAuthenticated, productController.uploadImage);

module.exports = router;
