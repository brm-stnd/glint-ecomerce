var express = require('express');
var router = express.Router();
var purchaseController = require('../../../controllers/api/v1/purchaseController.js');
const auth = require('../../../middleware/auth');

router.post('/cart/', auth.isAuthenticated, purchaseController.insertCart)
    .get('/cart/', auth.isAuthenticated, purchaseController.getCart)
router.delete('/cart/:id', auth.isAuthenticated, purchaseController.deleteCart)
router.put('/cart/:id', auth.isAuthenticated, purchaseController.updateCart)

router.post('/', auth.isAuthenticated, purchaseController.insertPurchase)
router.get('/history', auth.isAuthenticated, purchaseController.getHistoryPurchase)

module.exports = router;
