const mongoose      = require("mongoose")
const Schema        = mongoose.Schema

const cartSchema = new Schema({
    id_users: [{
        type: Schema.Types.ObjectId, 
        ref: 'Users'
    }],
    id_product: [{
        type: Schema.Types.ObjectId, 
        ref: 'Product'
    }],
    qty: {
        type: Number,
        required: true
    },
    date_input: { type: Date, default: Date.now },
}, { collection: 'cart' });

var Cart = mongoose.model("Cart", cartSchema);
module.exports = Cart;