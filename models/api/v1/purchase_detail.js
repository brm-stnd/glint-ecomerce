const mongoose      = require("mongoose")
const Schema        = mongoose.Schema

const purchaseDetailSchema = new Schema({
    id_users: [{
        type: Schema.Types.ObjectId, 
        ref: 'Users'
    }],
    id_product: [{
        type: Schema.Types.ObjectId, 
        ref: 'Product'
    }],
    qty: {
        type: Number,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    sub_total: {
        type: Number,
        required: true
    },
    date_input: { type: Date, default: Date.now },
}, { collection: 'purchase_detail' });

var Purchase_detail = mongoose.model("Purchase_detail", purchaseDetailSchema);
module.exports = Purchase_detail;