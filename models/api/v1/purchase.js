const mongoose      = require("mongoose")
const Schema        = mongoose.Schema

const purchaseSchema = new Schema({
    purchase_no: {
        type: String,
        required: true
    },
    id_users: [{
        type: Schema.Types.ObjectId, 
        ref: 'Users'
    }],
    total_purchase: {
        type: Number,
        required: true
    },
    payment: {
        type: Number,
        required: true
    },
    list_detail_purchase: [{
        type: Schema.Types.ObjectId, 
        ref: 'Purchase_detail'
    }],
    status: {
        type: String,
        enum : ['process','shiping','finished'],
        default: 'process'
    },
    date_input: { type: Date, default: Date.now },
}, { collection: 'purchase' });

var Purchase = mongoose.model("Purchase", purchaseSchema);
module.exports = Purchase;