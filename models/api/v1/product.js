const mongoose      = require("mongoose")
const Schema        = mongoose.Schema
var uniqueValidator = require('mongoose-unique-validator')

const productSchema = new Schema({
    name_product: {
        type: String,
        required: true
    },
    desc_product: {
        type: String
    },
    author: {
        type: String
    },
    picture: {
        type: String
    },
    price: {
        type: Number,
        required: true
    },
    stock: {
        type: Number,
        required: true
    },
    list_users: [{
        type: Schema.Types.ObjectId, 
        ref: 'Users'
    }],
    date_input: { type: Date, default: Date.now },
}, { collection: 'product' });

productSchema.plugin(uniqueValidator); 

var Product = mongoose.model("Product", productSchema);
module.exports = Product;