var FuncHelpers     = require('../../../helpers/response')
var Users           = require('../../../models/api/v1/users')
var Product         = require('../../../models/api/v1/product')
const Multer        = require('multer')
var ImageKit        = require('imagekit')

const storage = Multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/upload')
    },
    filename: function (req, file, cb) {
        var filetype = '';
        if (file.mimetype === 'image/gif') {
            filetype = 'gif';
        }
        else if (file.mimetype === 'image/png') {
            filetype = 'png';
        }
        else if (file.mimetype === 'image/jpeg') {
            filetype = 'jpg';
        }
        cb(null, file.fieldname + '-' + Date.now()+'.'+filetype)
    }
})
 
var upload = Multer().single('picture')

var imagekit = new ImageKit({
    "imagekitId" : process.env.IMAGEKIT_ID,       
    "apiKey"     : process.env.IMAGEKIT_PUBLIC_KEY,       
    "apiSecret"  : process.env.IMAGEKIT_SECRET_KEY, 
});

exports.uploadImage = function(req, res){
    if(req.decoded.user_type!=='merchant') return res.status(401).json(FuncHelpers.errorResponse("Just for mercant"))
    
    upload(req, res, function(err){
        var file = req.file
        if (!file) {
            return res.status(422).json(FuncHelpers.errorResponse("please upload file"))
        }

        var uploadPromise;
        uploadPromise = imagekit.upload(req.file.buffer.toString('base64'), {
            "filename" : req.file.originalname,
            "folder"   : "/product"
        });
    
        //handle upload success and failure
        uploadPromise.then((result)=>{
            Product.findByIdAndUpdate(req.params.id,{picture:result.url}, (err, updt)=>{
                if(err) return res.status(422).json(FuncHelpers.errorResponse("can't update url image")) 

                Product.findById(updt._id, (err, showUser)=>{
                    if(err) return res.status(422).json(FuncHelpers.errorResponse("can't update url image"))   
                    
                    res.status(200).json(FuncHelpers.successResponse("URL image has been update"))
                })
            })     
        })
        .catch (err=>{
            res.status(400).json(error("can't upload file"))
        })
    })
}

exports.insertProduct = async (req, res)=>{
    try {
        if(req.decoded.user_type!=='merchant') return res.status(401).json(FuncHelpers.errorResponse("Just for mercant"))

        let user = await Users.findById(req.decoded._id)

        if(user){
            let product = await Product.create(req.body)

            user.list_products.push(product)
            await user.save()

            product.list_users.push(user)
            let prod_result = await product.save()

            let result = {
                _id             : prod_result._id,
                name_product    : prod_result.name_product
            }
            res.status(201).json(FuncHelpers.successResponse(result))
        }else{
            res.status(422).json(FuncHelpers.errorResponse("User not exist"))
        }
    } catch (err) {
        res.status(422).json(FuncHelpers.errorResponse(err))
    }
}

exports.getProduct = function(req, res){
    Product.find({}, (err, prod)=>{
        res.status(200).json(FuncHelpers.successResponse(prod))
    })
}

exports.getProductbyId = function(req, res){
    Product.find({ _id: req.params.id }, (err, prod)=>{
        res.status(200).json(FuncHelpers.successResponse(prod))
    })
}

exports.deleteProduct = async (req, res)=>{
    try {
        if(req.decoded.user_type!=='merchant') return res.status(401).json(FuncHelpers.errorResponse("Just for mercant"))

        var product = await Product.findById(req.params.id)
        
        if(product){
            if (product.list_users != req.decoded._id){
                return res.status(422).json(FuncHelpers.errorResponse('Is not your product'))
            }else{
                product = await Product.findByIdAndDelete(req.params.id)

                let prod_pict = product.picture

                if(prod_pict){
                    var ret = prod_pict.replace(process.env.IMAGEKIT_URL,'')
                    console.log(ret)
                    var deletePromise = imagekit.deleteFile(ret)
                    
                    deletePromise.then(function(resp) {
                        console.log("file has been removed")    
                    })
                    .catch (err=>{
                        return console.log("file can't be remove")
                    });
                }

                await Users.update({ _id: req.decoded._id }, { $pullAll: { list_products: [req.params.id] } }, 
                    { safe: true, multi:true })

                return res.status(200).json(FuncHelpers.successResponse("Product success fully deleted"))
            }
        }else{
            return res.status(422).json(FuncHelpers.errorResponse('Product not exist'))
        }
    } catch (err) {
        if(err.kind==='ObjectId'){
            res.status(422).json(FuncHelpers.errorResponse('Id product not exist'))
        }else{
            res.status(422).json(FuncHelpers.errorResponse(err))
        }
    }
}

exports.seedProduct = async (req, res)=>{
    try {
        if(req.decoded.user_type!=='merchant') return res.status(401).json(FuncHelpers.errorResponse("Just for mercant"))

        let user = await Users.findById(req.decoded._id)

        if(user){
            let data_books = [
                {
                    name_product: 'Bumi Manusia',
                    desc_product: 'Novel jadoel',
                    price: 120000,
                    stock: 200,
                    author: 'Pramudya Ananta Toer'
                },
                {
                    name_product: 'Anak Semua Bangsa',
                    desc_product: 'Novel jadoel',
                    price: 110000,
                    stock: 100,
                    author: 'Pramudya Ananta Toer'
                },
                {
                    name_product: 'Jejak Langkah',
                    desc_product: 'Novel jadoel',
                    price: 100000,
                    stock: 400,
                    author: 'Pramudya Ananta Toer'
                },
                {
                    name_product: 'Rumah Kaca',
                    desc_product: 'Novel jadoel',
                    price: 150000,
                    stock: 500,
                    author: 'Pramudya Ananta Toer'
                }
            ]

            let product = await Product.create(data_books)

            for(var i=0;i<product.length;i++){
                user.list_products.push(product[i]._id)
                await user.save()

                //console.log(user._id)
                Product.findByIdAndUpdate(product[i]._id, {list_users: user._id}, (err, updt)=>{
                    if(err) return res.status(422).json(FuncHelpers.errorResponse("can't add id users"))
                })
            }

            res.status(201).json(FuncHelpers.successResponse("Process successfully"))
        }else{
            res.status(422).json(FuncHelpers.errorResponse("User not exist"))
        }
    } catch (err) {
        res.status(422).json(FuncHelpers.errorResponse(err))
    }
}






