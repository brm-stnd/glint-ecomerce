var FuncHelpers     = require('../../../helpers/response')
var Users           = require('../../../models/api/v1/users')
var Product         = require('../../../models/api/v1/product')
var Cart            = require('../../../models/api/v1/cart')
var Purchase_detail = require('../../../models/api/v1/purchase_detail')
var Purchase        = require('../../../models/api/v1/purchase')

exports.insertCart = async (req, res)=>{
    try {
        if(req.decoded.user_type!=='customer') return res.status(401).json(FuncHelpers.errorResponse("Just for customer"))

        var data_cart = req.body;
        data_cart.id_users = await req.decoded._id;   

        var cart = await Cart.find({ $and:[ {id_users:req.decoded._id}, {id_product:req.body.id_product}]})
        cart = cart[0]

        if(cart){
            let cart_qty = Number(cart.qty)+Number(req.body.qty)
            data_cart = {
                qty: cart_qty
            }

            cart = await Cart.findOneAndUpdate({ $and:[ {id_users:req.decoded._id}, {id_product:req.body.id_product}]}, data_cart)
        }else{
            cart = await Cart.create(data_cart)
        }

        res.status(201).json(FuncHelpers.successResponse(cart))
    } catch (err) {
        res.status(422).json(FuncHelpers.errorResponse(err))
    }
}

exports.getCart = function(req, res){
    if(req.decoded.user_type!=='customer') return res.status(401).json(FuncHelpers.errorResponse("Just for customer"))

    Cart.find({ id_users: req.decoded._id }).populate('id_product', 'name_product price')
        .then((cart)=>{
            
            let length = cart.length
            var list_cart = []
            var total_price = 0;
            for (var i = 0; i < length; i++) {

                var item = {
                    _id:            cart[i]._id,
                    name_product:   cart[i].id_product[0].name_product,
                    price:          cart[i].id_product[0].price,
                    qty:            cart[i].qty,
                    sub_total:      cart[i].id_product[0].price*cart[i].qty
                }

                total_price += cart[i].id_product[0].price*cart[i].qty

                list_cart.push(item)
            }

            var result = [{
                total_price: total_price,
                list_cart : list_cart
            }]

            res.status(200).json(FuncHelpers.successResponse(result));
        })
        .catch((err)=>{
            res.status(422).json(FuncHelpers.errorResponse(err));
        });
}

exports.updateCart = function(req, res, next){  
    if(req.decoded.user_type!=='customer') return res.status(401).json(FuncHelpers.errorResponse("Just for customer"))

    let id          = req.params.id;
    let data_update = {
        qty: req.body.qty
    }
    Cart.findOneAndUpdate({"_id":id}, data_update).exec()
        .then((cart)=>{
            res.status(200).json(FuncHelpers.successResponse(cart));
        })
        .catch((err)=>{
            res.status(422).json(FuncHelpers.errorResponse(err));
        });
}

exports.deleteCart = function(req, res) {
    if(req.decoded.user_type!=='customer') return res.status(401).json(FuncHelpers.errorResponse("Just for customer"))

    let id          = req.params.id; 
    Cart.findByIdAndRemove(id).exec()
        .then((cart)=>{
            res.status(200).json(FuncHelpers.successResponse(cart));
        })
        .catch((err)=>{
            res.status(422).json(FuncHelpers.errorResponse(err));
        });
}

exports.insertPurchase = async (req, res)=>{
    try {
        if(req.decoded.user_type!=='customer') return res.status(401).json(FuncHelpers.errorResponse("Just for customer"))

        let cart = await Cart.find({ id_users: req.decoded._id }).populate('id_product', 'name_product price')
        let length = cart.length

        if(length>0){

            var prod = '';
            var list_cart = []
            var item = {}
            var total_purchase = 0;
            for (var i = 0; i < length; i++) {

                prod = await Product.findById(cart[i].id_product)
                if(cart[i].qty>prod.stock){
                    return res.status(422).json(FuncHelpers.errorResponse("Insufficient stock of "+prod.name_product))
                }

                item = {
                    id_users:       req.decoded._id,
                    id_product:     cart[i].id_product,
                    qty:            cart[i].qty,
                    price:          cart[i].id_product[0].price,
                    sub_total:      cart[i].id_product[0].price*cart[i].qty
                }

                total_purchase += cart[i].id_product[0].price*cart[i].qty

                list_cart.push(item)
            }
            var purchase_detail = await Purchase_detail.create(list_cart);

            if(total_purchase>req.body.payment){
                return res.status(422).json(FuncHelpers.errorResponse("Your payment is not enough"))
            }
            
            var data_update_stock = {};
            var curr_stock = 0;
            var list_detail_purchase = []
            for (var i = 0; i < length; i++) {
                list_detail_purchase.push(purchase_detail[i]._id)

                //minus stock to Product
                prod = await Product.findById(cart[i].id_product)
                curr_stock = Number(prod.stock)-Number(cart[i].qty)
                
                data_update_stock = {
                    stock:  curr_stock
                }
                await Product.findOneAndUpdate({"_id":cart[i].id_product}, data_update_stock)
                //end minus stock to Product
            }

            var today = new Date();
            let purchase_no = 'INV'+today.getFullYear().toString().substr(-2)+(today.getMonth()+1)+today.getDate()+today.getSeconds();
            var purchase_main = {
                purchase_no:            purchase_no,
                id_users:               req.decoded._id,
                total_purchase:         total_purchase,
                payment:                req.body.payment,
                list_detail_purchase:   list_detail_purchase
            }
            await Purchase.create(purchase_main);

            await Cart.deleteMany({id_users: req.decoded._id})

            return res.status(201).json(FuncHelpers.successResponse("Purchase successed"))
        }else{
            return res.status(422).json(FuncHelpers.errorResponse("Chart is empty"))
        }
    } catch (err) {
        res.status(422).json(FuncHelpers.errorResponse(err))
    }
}

exports.getHistoryPurchase = function(req, res){
    if(req.decoded.user_type!=='customer') return res.status(401).json(FuncHelpers.errorResponse("Just for customer"))

    Purchase.find({ id_users: req.decoded._id }).populate('list_detail_purchase')
        .then((purchase)=>{

            res.status(200).json(FuncHelpers.successResponse(purchase));
        })
        .catch((err)=>{
            res.status(422).json(FuncHelpers.errorResponse(err));
        });
}